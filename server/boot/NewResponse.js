module.exports = function(server) {
  var remotes = server.remotes();


	var Account = server.models.Account;
  // modify all returned values

	remotes.before('**', function (ctx, next) {


		if (ctx.req.baseUrl.toString() == '/api/Vendors' || ctx.req.baseUrl.toString() == '/api/Customers' && ctx.req.method.toString() == 'POST') {

		}
		next();

	});

	remotes.afterError('**', function(ctx,next) {
	  ctx.result = {
		  status:"failure",
		  message:ctx.error.code,
		  data: ctx.result
		};
		next();
	});
  
  remotes.after('**', function (ctx, next) {


	if (ctx.req.baseUrl.toString() == '/api/Vendors' || ctx.req.baseUrl.toString() == '/api/Customers' && ctx.req.method.toString() == 'POST') {

		if(ctx.req.originalUrl.toString().indexOf('/api/Customers/logout') == 0 || ctx.req.originalUrl.toString().indexOf('/api/Vendors/logout') == 0 ||
                   ctx.req.originalUrl.toString().indexOf('/api/Customers/reset') == 0 || ctx.req.originalUrl.toString().indexOf('/api/Vendors/reset') == 0 )
		{
			//do nothing
			//console.log('logout url');
		}
		else
		{
		  var userType = '';
		  if(ctx.req.baseUrl.toString() == '/api/Vendors') {
			  userType = 'Vendor';
			  Account.create({'userId':ctx.result.userId, 'userType':userType, 'email': ctx.req.body.email}, function () {
				  //console.log('account is created');
			  });
		  } 
		  if(ctx.req.baseUrl.toString() == '/api/Customers') {
			  userType = 'Customer';
			  Account.create({'userId':ctx.result.userId, 'userType':userType, 'email': ctx.req.body.email}, function () {
				  //console.log('account is created');
			  });
		  }
               }

	  }


	ctx.result = {
      status:"success",
	  message:"",
	  data: ctx.result
    };
    next();
  });
  
};
