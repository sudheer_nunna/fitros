var server = require('../../server/server');


module.exports = function(Account) {

    Account.customImplementation = function (cb) {


        var Customer = server.models.Customer;
        Customer.find({}, function(err, customers){

            for(var c in customers) {

                var customer = customers[c];

                Account.create({'userId':customer.id, 'userType':"Customer", 'email': customer.email}, function () {
                    //console.log('account is created');
                });

            }

        });

        var Vendor = server.models.Vendor;
        Vendor.find({}, function(err, vendors){

            for(var v in vendors) {

                var vendor = vendors[v];

                Account.create({'userId':vendor.id, 'userType':"Vendor", 'email': vendor.email}, function () {
                    //console.log('account is created');
                });

            }

        });

    }

    Account.remoteMethod('customImplementation', {
        description: "Gets all subscription information bases on business name",
        returns: {
            arg: 'subscriptions',
            type: 'array'
        },
        accepts: {http: {source: 'query'}},
        http: {
            path: '/customImplementation',
            verb: 'get'
        }
    });

};
